﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main()
        {
            while (true)
            {
                Console.WriteLine("Enter max value for the range [1...] you'd like to check if that's numbers can reach 4-2-1 sequence");
                Console.WriteLine("Start with 50 or greater");
                string maxValueInput = Console.ReadLine();

                long maxValue = 0;

                if (long.TryParse(maxValueInput, NumberStyles.Any, CultureInfo.InvariantCulture, out maxValue))
                {
                    Console.WriteLine("Look's like its not a number");
                }

                long step = 50;

                if (maxValue > 10000)
                {
                    step = maxValue/100;
                }

                var result = new ConcurrentBag<KeyValuePair<long, long>>();
                var inputArray = GetInputArray(maxValue, step);

                // let's use more thay one thread for hard job
                Parallel.ForEach(inputArray, sourceItem =>
                {
                    // cycle for processing range of numbers
                    for (long i = sourceItem - step; i < sourceItem; i++)
                    {
                        long counter = 0;
                        long current = i;

                        // cycle for checking each particular number - will it reach 4-2-1 sequence or not?
                        while (true)
                        {
                            current = BruteForce(current);
                            counter++;

                            if (current == 1 || current == 2 || current == 4)
                            {
                                break;
                            }
                        }

                        //Console.WriteLine("{0}; {1}; tread_id {2}; number {0} reached '4-2-1' sequence in {1} steps", i, counter, Thread.CurrentThread.ManagedThreadId);
                        //result.Add(new KeyValuePair<long, string>(i, string.Format("{0}; {1}; number {0} reached '4-2-1' sequence in {1} steps", i, counter)));

                        // here we collecting data in pairs:
                        //    first     : particular number that we're trying to process
                        //    second    : number of steps we should take to reach 4-2-1 sequence
                        result.Add(new KeyValuePair<long, long>(i, counter));

                    }
                });

                // data transformations, sorting, grouping..
                var pairs = result
                    .OrderByDescending(x => x.Value)
                    .GroupBy(item => item.Value,
                        (key, values) =>
                            new KeyValuePair<long, long[]>(key, values.Select(i => i.Key).OrderBy(x => x).ToArray()))
                    .OrderBy(x => x.Key)
                    .ToDictionary(x => x.Key, y => y.Value);

                // pring results
                foreach (var pair in pairs)
                {
                    if (maxValue <= 1000)
                    {
                        Console.WriteLine("in {0} step sequence 1-2-4 can reach such numbers: {1}", pair.Key,
                            PrintArray(pair.Value));
                    }
                    else
                    {
                        Console.WriteLine("in {0} step sequence 1-2-4 can reach such count of numbers: {1}", pair.Key,
                            pair.Value.Length);
                    }
                }

                Console.WriteLine();
            }
        }

        /// <summary>
        /// 3N+1 sequence calculator. 
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public static
            long BruteForce(long i)
        {
            if (i == 0)
                return 1;

            if (i%2 == 0)
                return i/2;
            else
                return 3*i + 1;
        }

        /// <summary>
        /// Helper method. Used for input data preparation
        /// </summary>
        /// <param name="maxValue"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public static long[] GetInputArray(long maxValue, long step)
        {
            var result = new List<long>();
            var count = 1;

            while (step*count <= maxValue)
            {
                result.Add(step*count);
                count++;
            }

            return result.ToArray();
        }

        /// <summary>
        /// Helper method. Can be used to print array as a string
        /// </summary>
        /// <param name="arrayLongs"></param>
        /// <returns></returns>
        public static string PrintArray(long[] arrayLongs)
        {
            var sb = new StringBuilder();
            foreach (var arrayLong in arrayLongs)
            {
                sb.AppendFormat("{0},", arrayLong);
            }

            return String.Format("[{0}]", sb.ToString(0, sb.Length - 1));
        }
    }
}
